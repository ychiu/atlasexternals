# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  IDNA_PYTHON_PATH
#
# Can be steered by IDNA_ROOT.
#

# The LCG include(s):
include( LCGFunctions )

# If it was already found, let's be quiet:
if( IDNA_FOUND )
   set( idna_FIND_QUIETLY TRUE )
endif()

# Ignore system paths when an LCG release was set up:
if( IDNA_ROOT )
   set( _extraIdnaArgs NO_SYSTEM_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH )
endif()

# Find the python path:
find_path( IDNA_PYTHON_PATH 
   NAMES idna/__init__.py
   PATH_SUFFIXES lib/python2.7/site-packages
   PATHS ${IDNA_ROOT}
   ${_extraIdnaArgs} )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( idna DEFAULT_MSG
   IDNA_PYTHON_PATH )

# Set up the RPM dependency:
lcg_need_rpm( idna )

# Clean up:
if( _extraIdnaArgs )
   unset( _extraIdnaArgs )
endif()
