# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building (downloading) DCAP as part of the analysis release.
#

# Set the name of the package:
atlas_subdir( dcap )

# In release recompilation mode stop now:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_DCAP )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building DCAP as part of this project" )

# The sources/binaries of DCAP:
set( _sourceSLC6
   "http://cern.ch/lcgpackages/tarFiles/sources/dcap-2.47.7-1-x86_64-slc6.tar.gz" )
set( _md5SLC6 "246930601e1636219afa63be2e7b98f4" )

set( _sourceCC7
   "http://cern.ch/lcgpackages/tarFiles/sources/dcap-2.47.7-1-x86_64-centos7.tar.gz" )
set( _md5CC7 "246930601e1636219afa63be2e7b98f4" )

# Select which version to use:
atlas_os_id( _os _isValid )
if( NOT _isValid )
   message( WARNING "Could not determine OS name. DCAP will not be built!" )
   return()
endif()
if( "${_os}" STREQUAL "slc6" )
   set( _source "${_sourceSLC6}" )
   set( _md5    "${_md5SLC6}" )
elseif( "${_os}" STREQUAL "centos7" )
   set( _source "${_sourceCC7}" )
   set( _md5    "${_md5CC7}" )
else()
   message( WARNING "DCAP not available for OS: ${_os}" )
   return()
endif()

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/DCAPBuild )

# Build DCAP:
ExternalProject_Add( dcap
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_source}
   URL_MD5 ${_md5}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo "No configuration for DCAP"
   BUILD_COMMAND ${CMAKE_COMMAND} -E echo "No build for DCAP"
   INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> ${_buildDir}
   COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR> )
add_dependencies( Package_dcap dcap )

# Install DCAP:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Set up the runtime environment for it:
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/dcapEnvironmentConfig.cmake.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/dcapEnvironmentConfig.cmake
   @ONLY )
set( dcapEnvironment_DIR ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}
   CACHE PATH "Location of dcapEnvironmentConfig.cmake" )
find_package( dcapEnvironment )
