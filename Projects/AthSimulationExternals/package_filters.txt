# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# List of packages to build as part of AthSimulationExternals.
#
+ External/Blas
+ External/CLHEP
+ External/FastJet
+ External/GPerfTools
+ External/Gdb
+ External/Geant4
+ External/GoogleTest
+ External/HepMCAnalysis
+ External/Lapack
+ External/MKL
+ External/dSFMT
+ External/yampl
- .*
